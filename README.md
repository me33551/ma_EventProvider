# ma_EventProvider

After cloning the repository and installing ruby along with the required gems the file permissions need to be changed so that "server15.rb" is executable (command "chmod +x server15.rb" on Linux)

The server relies on the redis database being accessible via unix socket and containing the process traces in database 15 - therefore, redis needs to be installed, started and containing the traces (see also [ProcessCreation](https://gitlab.com/me33551/ma_ProcessCreation))

Afterwards, the server needs to be started with the command "./server15.rb start -v"