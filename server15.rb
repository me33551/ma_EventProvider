#!/usr/bin/ruby
require 'pp'
require 'riddl/server'
require 'json'
require 'redis'

class Bar < Riddl::Implementation
  def response
    Riddl::Parameter::Complex.new("return","text/plain","hello world #{@r[0]}")
  end
end

class List < Riddl::Implementation
  def response
    puts "Connection established"
    redis=Redis.new(
      #:host => '127.0.0.1',
      #:port => 6379,
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    puts "Redis connection established"
    redis.select(15)
    keys=redis.keys('*')
    Riddl::Parameter::Complex.new("return","application/json",keys.to_json)
  end
end

class ListWithHierarchy < Riddl::Implementation
  def response
    puts "Connection established"
    redis=Redis.new(
      #:host => '127.0.0.1',
      #:port => 6379,
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    puts "Redis connection established"
    redis.select(15)
    keys=redis.keys('*')
    resp=[]
    keys.each do |key|
      pp key
      subtraces=[]
      jsons=redis.lrange(key,0,-1)
      trace=nil
      if(!jsons.empty? && JSON.parse(jsons[0]).key?('log')) then
        trace=jsons.shift
      end
      jsons.each do |json|
=begin
        object = JSON.parse(json)
        if object.dig('event','cpee:lifecycle:transition')=='task/instantiation' && object.dig('event', 'data', 'data_receiver').is_a?(Hash) && !object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID').nil? then
          subtraces.push(object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID'))
          pp "subtrace #{object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID')}"
        end
=end
        instantiation = json.match /\"cpee:lifecycle:transition\":\"task\/instantiation\"/
        matchData = json.match /\"CPEE-INSTANCE-UUID\":\"(.*?)\"/
        if !instantiation.nil? && !matchData.nil?
#        if !instantiation.nil? && !matchData.nil? && !subtraces.include?(matchData[1])
          subtraces.push matchData[1]
          pp "subtrace #{matchData[1]}"
        end
=begin
        matchData = json.match /\"CPEE-INSTANCE-UUID\":\"(.*?)\"/
        if !matchData.nil? && json.match?(/\"CPEE-STATE\":\"running\"/)
          subtraces.push matchData[1]
          pp "subtrace #{matchData[1]}"
        end
=end
      end
      traceObject = JSON.parse(trace)
      resp.push( {:uuid => key, :subtraces => subtraces, :id => traceObject['log']['trace']['concept:name'], :name => traceObject['log']['trace']['cpee:name'] } )
    end
    #Riddl::Parameter::Complex.new("return","application/json",keys.to_json)
    Riddl::Parameter::Complex.new("return","application/json",resp.to_json)
  end
end

class StreamSSE < Riddl::SSEImplementation
  def onopen
    puts "Connection established"
    redis=Redis.new(
      #:host => '127.0.0.1',
      #:port => 6379,
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    puts "Redis connection established"
    redis.select(15)
    #jsons=redis.lrange('037e3bde-309e-4ca2-bfd5-a38dde59053d', 0, -1)
    jsons=redis.lrange(@r[1], 0, -1)
    if(!jsons.empty? && JSON.parse(jsons[0]).key?('log')) then
      trace=jsons.shift
    end
    Thread.new do
      jsons.each do |json|
        send(json)
      end
      close
    end
  end

  def onclose
    puts "Connection closed"
  end
end

def getSubTracesSSE(traceUuid, redis)
  jsons=redis.lrange(traceUuid, 0, -1)
  if(!jsons.empty? && JSON.parse(jsons[0]).key?('log')) then
    trace=jsons.shift
  end
  #Thread.new do
    jsons.each do |json|
      send(json)
      instantiation = json.match /\"cpee:lifecycle:transition\":\"task\/instantiation\"/
      matchData = json.match /\"CPEE-INSTANCE-UUID\":\"(.*?)\"/
      if !instantiation.nil? && !matchData.nil?
        getSubTracesSSE(matchData[1],redis)
      end
    end
  #end
end
class StreamSSEIncludingSubTraces < Riddl::SSEImplementation
  def onopen
    puts "Connection established"
    redis=Redis.new(
      #:host => '127.0.0.1',
      #:port => 6379,
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    puts "Redis connection established"
    redis.select(15)
#    Thread.new do
      getSubTracesSSE(@r[1],redis)
#      close
#    end
  end

  def onclose
    puts "Connection closed"
  end
end

def getSubTracesPubSub(traceUuid, redis, publishChannel, level)
  jsons=redis.lrange(traceUuid, 0, -1)
  if(!jsons.empty? && JSON.parse(jsons[0]).key?('log')) then
    trace=jsons.shift
  end
  #Thread.new do
    jsons.each do |json|
      #send(json)
      redis.publish(publishChannel, json)
      instantiation = json.match /\"cpee:lifecycle:transition\":\"task\/instantiation\"/
      matchData = json.match /\"CPEE-INSTANCE-UUID\":\"(.*?)\"/
      if !instantiation.nil? && !matchData.nil?
        #Thread.new do
          getSubTracesPubSub(matchData[1],redis, publishChannel, level+1)
        #end
      end
      #sleep 1
      #sleep 0.1
      #sleep 0.01
    end
    if level==0 && traceUuid != 'live' then
      redis.publish(publishChannel, "when does this finally finish?!")
    end
  #end
end
class PubSubIncludingSubTraces < Riddl::Implementation
  def response
    puts "Connection established"
    redis=Redis.new(
      #:host => '127.0.0.1',
      #:port => 6379,
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    puts "Redis connection established"
    redis.select(15)
    Thread.new do
      getSubTracesPubSub(@r[1], redis, @r[1], 0)
    end
    Riddl::Parameter::Complex.new("return","application/json", ({:result => 'hier gibts nichts zu sehen - bitte weitergehen'}).to_json)
  end
end


class Events < Riddl::Implementation
  def response
    puts "Connection established"
    redis=Redis.new(
      #:host => '127.0.0.1',
      #:port => 6379,
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    puts "Redis connection established"
    redis.select(15)
    jsons=redis.lrange(@r[1], 0, -1)
    if(!jsons.empty? && JSON.parse(jsons[0]).key?('log')) then
      trace=jsons.shift
    end
    Riddl::Parameter::Complex.new("return","application/json", ([trace]+jsons).to_json)
  end
end

class EventsBeautiful < Riddl::Implementation
  def response
    puts "Connection established"
    redis=Redis.new(
      #:host => '127.0.0.1',
      #:port => 6379,
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    puts "Redis connection established"
    redis.select(15)
    jsons=redis.lrange(@r[1], 0, -1)
    if(!jsons.empty? && JSON.parse(jsons[0]).key?('log')) then
      trace=JSON.parse(jsons.shift)
    end
    jsons=jsons.map do |json|
      JSON.parse(json)
    end
    Riddl::Parameter::Complex.new("return","application/json", ([trace]+jsons).to_json)
  end
end

class ListenPubSub < Riddl::SSEImplementation
  def onopen
    puts "Connection established"
    redis=Redis.new(
      #:host => '127.0.0.1',
      #:port => 6379,
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    puts "Redis connection established"
    redis.select(15)
    Thread.new do
      redis.subscribe(@r[1]) do |on|
        on.subscribe do |channel, subscriptions|
          puts "Subscribed to #{channel} (#{subscriptions} subscriptions)"
        end
        on.message do |channel, msg|
#          puts "#{channel} - [#{msg}]"
          if msg=="when does this finally finish?!" then 
            redis.unsubscribe(@r[1])
          else
            send(msg)
          end
        end
        on.unsubscribe do |channel, subscriptions|
          puts "Unsubscribed from #{channel} (#{subscriptions} subscriptions)"
          close
        end
      end
    end
  end

  def onclose
    puts "Connection closed"
  end
end

class ReplayPubSub < Riddl::Implementation
  def response
    puts "Connection established"
    redis=Redis.new(
      #:host => '127.0.0.1',
      #:port => 6379,
      :path => "/tmp/redis.sock",
      :encoding => 'utf-8'
    )
    puts "Redis connection established"
    redis.select(15)
    jsons=redis.lrange(@r[1], 0, -1)
    if(!jsons.empty? && JSON.parse(jsons[0]).key?('log')) then
      trace=jsons.shift
    end
    Thread.new do
      jsons.each do |json|
        redis.publish(@r[1], json)
        #sleep 0.1
      end
      redis.publish(@r[1], 'when does this finally finish?!')
    end
    Riddl::Parameter::Complex.new("return","application/json", ([trace]+jsons).to_json)
  end
end

Riddl::Server.new(File.dirname(__FILE__) + '/description.xml', :port => 9314) do
  cross_site_xhr true

  on resource do
    run Bar if get '*'
    run Echo if sse
    on resource 'traces' do
      run List if get '*'
      on resource do
        run Events if get '*'
        on resource 'sse' do
          run Bar if get '*'
          run StreamSSE if sse
        end
        on resource 'pubsub' do
          run ReplayPubSub if get '*'
          run ListenPubSub if sse
        end
        on resource 'beautiful' do
          run EventsBeautiful if get '*'
        end
        on resource 'SSEwithSub' do
          run StreamSSEIncludingSubTraces if sse
        end
        on resource 'pubsubWithSub' do
          run PubSubIncludingSubTraces if get '*'
        end
      end
    end
    on resource 'tracesWithoutEventListWithHierarchy' do
      run ListWithHierarchy if get '*'
    end
  end
end.loop!
